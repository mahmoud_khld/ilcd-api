package de.fzk.iai.ilcd.api.binding.helper;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.URL;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.PropertyException;

import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.xml.sax.InputSource;

import de.fzk.iai.ilcd.api.dataset.ILCDTypes;

public abstract class AbstractHelper {
	
	protected final Logger log = org.apache.log4j.Logger.getLogger(AbstractHelper.class);

	public AbstractHelper() {
		super();
	}

	protected Marshaller createMarshaller( ILCDTypes type ) throws JAXBException {
		JAXBContext jc = ContextFactory.getInstance().getContext(type);
	
		Marshaller marshaller = jc.createMarshaller();
	
		setNameSpacePrefixMapper(marshaller, type);
	
		return marshaller;
	}

	protected void setNameSpacePrefixMapper(Marshaller m, ILCDTypes type) {
		try {
			m.setProperty("com.sun.xml.bind.namespacePrefixMapper", new NamespacePrefixMapperImpl(type));
		} catch (PropertyException e) {
			log.warn("WARNING: prefix mapper assignment failed. " + e.getMessage());
			// if the JAXB provider doesn't recognize the prefix mapper,
			// it will throw this exception. Since being unable to specify
			// a human friendly prefix is not really a fatal problem,
			// we can just continue marshalling without failing
		}
	}

	protected InputSource wrapInputStream( InputStream stream ) throws UnsupportedEncodingException {
		Reader reader = new InputStreamReader(stream, "UTF-8");
		InputSource is = new InputSource(reader);
		is.setEncoding("UTF-8");
		return is;
	}

	public Document parse( InputStream stream ) {
		Document result = null;
		try {
			SAXBuilder builder = new SAXBuilder();
			result = builder.build(wrapInputStream(stream));
	
			// WhiteSpaceFilterInputStream(stream)));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JDOMException e) {
			e.printStackTrace();
		}
		return result;
	}

	public Document parse( URL url ) throws FileNotFoundException {
		Document result = null;
		try {
			result = parse(url.openStream());
		} catch (FileNotFoundException e) {
			throw e;
		} catch (IOException e) {
			log.error(e);
		}
		return result;
	}

}