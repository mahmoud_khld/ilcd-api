//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.2-34 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2010.03.01 at 01:55:00 PM MEZ 
//

package de.fzk.iai.ilcd.api.binding.generated.contact;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import de.fzk.iai.ilcd.api.app.contact.AdministrativeInformation;
import de.fzk.iai.ilcd.api.app.contact.ContactDataSet;
import de.fzk.iai.ilcd.api.app.contact.ContactInformation;
import de.fzk.iai.ilcd.api.app.contact.DataEntryBy;
import de.fzk.iai.ilcd.api.app.contact.DataSetInformation;
import de.fzk.iai.ilcd.api.app.contact.PublicationAndOwnership;

/**
 * This object contains factory methods for each Java content interface and Java
 * element interface generated in the
 * de.fzk.iai.ilcd.api.binding.generated.contact package.
 * <p>
 * An ObjectFactory allows you to programatically construct new instances of the
 * Java representation for XML content. The Java representation of XML content
 * can consist of schema derived interfaces and classes representing the binding
 * of schema type definitions, element declarations and model groups. Factory
 * methods for each of these are provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

	private final static QName _ContactDataSet_QNAME = new QName("http://lca.jrc.it/ILCD/Contact", "contactDataSet");

	/**
	 * Create a new ObjectFactory that can be used to create new instances of
	 * schema derived classes for package:
	 * de.fzk.iai.ilcd.api.binding.generated.contact
	 * 
	 */
	public ObjectFactory() {
	}

	/**
	 * Create an instance of {@link AdministrativeInformationType }
	 * 
	 */
	public AdministrativeInformationType createAdministrativeInformationType() {
		return new AdministrativeInformation();
	}

	/**
	 * Create an instance of {@link ContactInformationType }
	 * 
	 */
	public ContactInformationType createContactInformationType() {
		return new ContactInformation();
	}

	/**
	 * Create an instance of {@link DataEntryByType }
	 * 
	 */
	public DataEntryByType createDataEntryByType() {
		return new DataEntryBy();
	}

	/**
	 * Create an instance of {@link PublicationAndOwnershipType }
	 * 
	 */
	public PublicationAndOwnershipType createPublicationAndOwnershipType() {
		return new PublicationAndOwnership();
	}

	/**
	 * Create an instance of {@link DataSetInformationType }
	 * 
	 */
	public DataSetInformationType createDataSetInformationType() {
		return new DataSetInformation();
	}

	/**
	 * Create an instance of {@link ContactDataSetType }
	 * 
	 */
	public ContactDataSetType createContactDataSetType() {
		return new ContactDataSet();
	}

	/**
	 * Create an instance of {@link JAXBElement }{@code <}
	 * {@link ContactDataSetType }{@code >}
	 * 
	 */
	@XmlElementDecl(namespace = "http://lca.jrc.it/ILCD/Contact", name = "contactDataSet")
	public JAXBElement<ContactDataSetType> createContactDataSet(ContactDataSetType value) {
		return new JAXBElement<ContactDataSetType>(_ContactDataSet_QNAME, ((Class) ContactDataSet.class), null, ((ContactDataSet) value));
	}

}
