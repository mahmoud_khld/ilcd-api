package de.fzk.iai.ilcd.api;

public class Constants {
	/*
	 * general
	 */
	public static final String API_ID = "ILCD API RI 0.4";

	public static final String FORMAT_VERSION = "1.1";

	public static final String DEFAULT_LANGUAGE = "en";

	public static final String DEFAULT_ENCODING = "UTF-8";

	/*
	 * paths
	 */
	public static final String ILCD_PATH_PREFIX = "de/fzk/iai/lca/ilcd/";

	public static final String SCHEMAS_PATH_PREFIX = ILCD_PATH_PREFIX + "schemas/";

	public static final String STYLESHEETS_PATH_PREFIX = ILCD_PATH_PREFIX + "stylesheets/";

	public static final String TOOLS_STYLESHEETS_PATH_PREFIX = ILCD_PATH_PREFIX + "tools/stylesheets/";

	public static final String DEFAULT_ILCD_LOCATIONS_NAME = "ILCDLocations.xml";

	public static final String DEFAULT_ILCD_LCIA_METHODOLOGIES_NAME = "ILCDLCIAMethodologies.xml";

	public static final String DEFAULT_ILCD_CATEGORIES_NAME = "ILCDClassification.xml";

	public static final String DEFAULT_ILCD_FLOW_CATEGORIES_NAME = "ILCDFlowCategorization.xml";

	/*
	 * migrate
	 */
	public static final String MIGRATE_1_1_STYLESHEET_NAME = "migrate_1.1.xsl";

	/*
	 * validation
	 */
	public static final String COMPLIANCE_STYLESHEET_NAME = "compliance_1.1.xsl";

	public static final String VALIDATE_STYLESHEET_NAME = "validate.xsl";

	public static final String DATASET_FORMAT_SOURCE_UUID = "a97a0155-0234-4b87-b4ce-a45da52f2a40";

	public static final String DATASET_FORMAT_SOURCE_VERSION = "01.01.000";

	public static final String DATASET_FORMAT_SOURCE_PATH = "../sources/a97a0155-0234-4b87-b4ce-a45da52f2a40.xml";

	public static final String COMPLIANCE_REFERENCE_CATEGORIES_FILE = "ILCDClassifiction_Reference_Compliance_1.1.xml";

	public static final String COMPLIANCE_REFERENCE_CATEGORIES_PATH = "de/fzk/iai/lca/ilcd/stylesheets/" + COMPLIANCE_REFERENCE_CATEGORIES_FILE;

	/*
	 * package names
	 */
	public static final String PACKAGE_GENERATED = "de.fzk.iai.ilcd.api.binding.generated.";

	public static final String PACKAGE_CATEGORIES = PACKAGE_GENERATED + "categories";

	public static final String PACKAGE_COMMON = PACKAGE_GENERATED + "common";

	public static final String PACKAGE_LOCATIONS = PACKAGE_GENERATED + "locations";

	public static final String PACKAGE_LCIAMETHODOLOGIES = PACKAGE_GENERATED + "lciamethodologies";

	public static final String PACKAGE_UNITGROUP = PACKAGE_GENERATED + "unitgroup";

	public static final String PACKAGE_SOURCE = PACKAGE_GENERATED + "source";

	public static final String PACKAGE_PROCESS = PACKAGE_GENERATED + "process";

	public static final String PACKAGE_LCIAMETHOD = PACKAGE_GENERATED + "lciamethod";

	public static final String PACKAGE_FLOWPROPERTY = PACKAGE_GENERATED + "flowproperty";

	public static final String PACKAGE_FLOW = PACKAGE_GENERATED + "flow";

	public static final String PACKAGE_CONTACT = PACKAGE_GENERATED + "contact";

	/*
	 * namespaces
	 */
	public static final String NS_SCHEMA_INSTANCE = "http://www.w3.org/2001/XMLSchema-instance";

	public static final String NS_PROCESS = "http://lca.jrc.it/ILCD/Process";

	public static final String NS_LCIAMETHOD = "http://lca.jrc.it/ILCD/LCIAMethod";

	public static final String NS_FLOW = "http://lca.jrc.it/ILCD/Flow";

	public static final String NS_FLOWPROPERTY = "http://lca.jrc.it/ILCD/FlowProperty";

	public static final String NS_UNITGROUP = "http://lca.jrc.it/ILCD/UnitGroup";

	public static final String NS_SOURCE = "http://lca.jrc.it/ILCD/Source";

	public static final String NS_CONTACT = "http://lca.jrc.it/ILCD/Contact";

	public static final String NS_COMMON = "http://lca.jrc.it/ILCD/Common";

	/*
	 * schema names
	 */
	public static final String PROCESS_SCHEMA_NAME = "ILCD_ProcessDataSet.xsd";

	public static final String LCIAMETHOD_SCHEMA_NAME = "ILCD_LCIAMethodDataSet.xsd";

	public static final String FLOW_SCHEMA_NAME = "ILCD_FlowDataSet.xsd";

	public static final String FLOW_PROPERTY_SCHEMA_NAME = "ILCD_FlowPropertyDataSet.xsd";

	public static final String UNIT_GROUP_SCHEMA_NAME = "ILCD_UnitGroupDataSet.xsd";

	public static final String SOURCE_SCHEMA_NAME = "ILCD_SourceDataSet.xsd";

	public static final String CONTACT_SCHEMA_NAME = "ILCD_ContactDataSet.xsd";

	/*
	 * display stylesheets
	 */
	public static final String LCIAMETHOD_STYLESHEET_NAME = "lciamethod2html.xsl";

	public static final String CONTACT_STYLESHEET_NAME = "contact2html.xsl";

	public static final String SOURCE_STYLESHEET_NAME = "source2html.xsl";

	public static final String UNITGROUP_STYLESHEET_NAME = "unitgroup2html.xsl";

	public static final String FLOWPROPERTY_STYLESHEET_NAME = "flowproperty2html.xsl";

	public static final String FLOW_STYLESHEET_NAME = "flow2html.xsl";

	public static final String PROCESS_STYLESHEET_NAME = "process2html.xsl";

	/*
	 * root elements
	 */
	public static final String LOCATIONS_ROOT_ELEMENT_NAME = "ILCDLocations";

	public static final String LCIAMETHODOLOGIES_ROOT_ELEMENT_NAME = "ILCDLCIAMethodologies";

	public static final String CATEGORIES_ROOT_ELEMENT_NAME = "CategorySystem";

	public static final String LCIAMETHOD_ROOT_ELEMENT_NAME = "LCIAMethodDataSet";

	public static final String CONTACT_ROOT_ELEMENT_NAME = "contactDataSet";

	public static final String SOURCE_ROOT_ELEMENT_NAME = "sourceDataSet";

	public static final String UNIT_GROUP_ROOT_ELEMENT_NAME = "unitGroupDataSet";

	public static final String FLOW_PROPERTY_ROOT_ELEMENT_NAME = "flowPropertyDataSet";

	public static final String FLOW_ROOT_ELEMENT_NAME = "flowDataSet";

	public static final String PROCESS_ROOT_ELEMENT_NAME = "processDataSet";

	/*
	 * reference types
	 */
	public static final String REFERENCE_TYPE_CONTACT = "contact data set";

	public static final String REFERENCE_TYPE_SOURCE = "source data set";

	public static final String REFERENCE_TYPE_UNIT_GROUP = "unit group data set";

	public static final String REFERENCE_TYPE_FLOW_PROPERTY = "flow property data set";

	public static final String REFERENCE_TYPE_FLOW = "flow data set";

	public static final String REFERENCE_TYPE_LCIA_METHOD = "LCIA method data set";

	public static final String REFERENCE_TYPE_PROCESS = "process data set";

	/*
	 * folder names
	 */
	public static final String FOLDER_NAME_CONTACT = "contacts";

	public static final String FOLDER_NAME_SOURCE = "sources";

	public static final String FOLDER_NAME_UNIT_GROUP = "unitgroups";

	public static final String FOLDER_NAME_FLOW_PROPERTY = "flowproperties";

	public static final String FOLDER_NAME_FLOW = "flows";

	public static final String FOLDER_NAME_LCIA_METHOD = "lciamethods";

	public static final String FOLDER_NAME_PROCESS = "processes";

	public static final String FOLDER_NAME_EXT_DOCS = "external_docs";

	/*
	 * extensions
	 */
	public static final String EXTENSION_PACKAGE_PRODUCTMODEL = "de.fzk.iai.ilcd.api.extension.productmodel";

	public static final String EXTENSION_NS_PRODUCTMODEL = "http://iai.kit.edu/ILCD/ProductModel";

	public static final String EXTENSION_PACKAGE_ORIGINATINGPROCESS = "de.fzk.iai.ilcd.api.extension.ef.originatingprocess";

	public static final String EXTENSION_NS_EF_2018 = "http://eplca.jrc.ec.europa.eu/ILCD/Extensions/2018/EF";

}
