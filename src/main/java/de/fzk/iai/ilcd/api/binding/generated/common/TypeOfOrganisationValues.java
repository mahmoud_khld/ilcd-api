//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.2-34 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2010.03.01 at 01:55:00 PM MEZ 
//

package de.fzk.iai.ilcd.api.binding.generated.common;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for TypeOfOrganisationValues.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * <p>
 * 
 * <pre>
 * &lt;simpleType name="TypeOfOrganisationValues"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Private company"/&gt;
 *     &lt;enumeration value="Governmental"/&gt;
 *     &lt;enumeration value="Non-governmental org."/&gt;
 *     &lt;enumeration value="Other"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "TypeOfOrganisationValues")
@XmlEnum
public enum TypeOfOrganisationValues {

	/**
	 * Private company
	 * 
	 */
	@XmlEnumValue("Private company")
	PRIVATE_COMPANY("Private company"),

	/**
	 * Governmental organisation
	 * 
	 */
	@XmlEnumValue("Governmental")
	GOVERNMENTAL("Governmental"),

	/**
	 * Non-governmental organisation
	 * 
	 */
	@XmlEnumValue("Non-governmental org.")
	NON_GOVERNMENTAL_ORG("Non-governmental org."),

	/**
	 * Other, e.g. a project consortium or network
	 * 
	 */
	@XmlEnumValue("Other")
	OTHER("Other");
	private final String value;

	TypeOfOrganisationValues(String v) {
		value = v;
	}

	public String value() {
		return value;
	}

	public static TypeOfOrganisationValues fromValue(String v) {
		for (TypeOfOrganisationValues c : TypeOfOrganisationValues.values()) {
			if (c.value.equals(v)) {
				return c;
			}
		}
		throw new IllegalArgumentException(v);
	}

	public String getValue() {
		return value;
	}

}
