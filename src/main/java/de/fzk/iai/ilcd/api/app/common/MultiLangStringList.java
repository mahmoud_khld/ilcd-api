package de.fzk.iai.ilcd.api.app.common;

import java.util.ArrayList;
import java.util.List;

import de.fzk.iai.ilcd.api.Configuration;

public class MultiLangStringList<T> extends ArrayList<T> implements List<T> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public String getValue() {
		return getValue(Configuration.getInstance().getLanguage());
	}

	public String getValue(String lang) {
		for (T item : this) {
			IMultiLang i = (IMultiLang) item;
			if (i.getLang().equals(lang))
				return i.getValue();
		}
		return null;
	}

	public void setValue(String value) {
		setValue(value, Configuration.getInstance().getLanguage());
	}

	@SuppressWarnings("unchecked")
	public void setValue(String value, String lang) {
		for (T item : this) {
			IMultiLang i = (IMultiLang) item;
			if (i.getLang().equals(lang)) {
				i.setValue(value);
				return;
			}

		}
		FTMultiLang newVal = new FTMultiLang();
		newVal.setLang(lang);
		newVal.setValue(value);

		this.add((T) newVal);
	}

}
