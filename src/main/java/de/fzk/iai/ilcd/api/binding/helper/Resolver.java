/**
 * 
 */
package de.fzk.iai.ilcd.api.binding.helper;

import java.io.InputStream;

import org.apache.log4j.Logger;
import org.w3c.dom.ls.LSResourceResolver;

import com.sun.org.apache.xerces.internal.dom.DOMInputImpl;

import de.fzk.iai.ilcd.api.Constants;

class Resolver implements LSResourceResolver {

	private Logger logger = Logger.getLogger(this.getClass());

	/**
	 * 
	 * @param str
	 * @param str1
	 * @param str2
	 * @param str3
	 * @param str4
	 * @return
	 */
	public org.w3c.dom.ls.LSInput resolveResource(String str, String str1, String str2, String str3, String str4) {
		logger.debug("resolving " + str3 + " to " + Constants.SCHEMAS_PATH_PREFIX + str3);
		InputStream is = this.getClass().getClassLoader().getResourceAsStream(Constants.SCHEMAS_PATH_PREFIX + str3);
		return new DOMInputImpl(str2, str3, str4, is, Constants.DEFAULT_ENCODING);
	}
}