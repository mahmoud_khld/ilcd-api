package de.fzk.iai.ilcd.api.app.common;

public interface IMultiLang {

	public abstract String getValue();

	public abstract String getLang();

	public abstract void setValue(String value);

	public abstract void setLang(String lang);

}
