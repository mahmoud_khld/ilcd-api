
//generated from reference_types.xml 
            
package de.fzk.iai.ilcd.api.mapper;
 
import de.fzk.iai.ilcd.api.dataset.ILCDTypes;
import de.fzk.iai.ilcd.api.binding.generated.common.GlobalReferenceTypeValues;            

public class GlobalReferenceDatasetTypeMapper {
            
    public static GlobalReferenceTypeValues getType(String referenceName, ILCDTypes type) {

        if (referenceName.equals("referenceToCommissioner"))
            return GlobalReferenceTypeValues.fromValue("contact data set");
        else if (referenceName.equals("referenceToComplementingProcess"))
            return GlobalReferenceTypeValues.fromValue("process data set");
        else if (referenceName.equals("referenceToCompleteReviewReport"))
            return GlobalReferenceTypeValues.fromValue("source data set");
        else if (referenceName.equals("referenceToComplianceSystem"))
            return GlobalReferenceTypeValues.fromValue("source data set");
        else if (referenceName.equals("referenceToContact"))
            return GlobalReferenceTypeValues.fromValue("contact data set");
        else if (referenceName.equals("referenceToConvertedOriginalDataSetFrom"))
            return GlobalReferenceTypeValues.fromValue("source data set");
        else if (referenceName.equals("referenceToDataHandlingPrinciples"))
            return GlobalReferenceTypeValues.fromValue("source data set");
        else if (referenceName.equals("referenceToDataSetFormat"))
            return GlobalReferenceTypeValues.fromValue("source data set");
        else if (referenceName.equals("referenceToDataSetUseApproval"))
            return GlobalReferenceTypeValues.fromValue("contact data set");
        else if (referenceName.equals("referenceToDataSource"))
            return GlobalReferenceTypeValues.fromValue("source data set");
        else if (referenceName.equals("referenceToEntity"))
            return GlobalReferenceTypeValues.fromValue("contact data set");
        else if (referenceName.equals("referenceToEntitiesWithExclusiveAccess"))
            return GlobalReferenceTypeValues.fromValue("contact data set");
        else if (referenceName.equals("referenceToExternalDocumentation"))
            return GlobalReferenceTypeValues.fromValue("source data set");
        else if (referenceName.equals("referenceToFlowDataSet"))
            return GlobalReferenceTypeValues.fromValue("flow data set");
        else if (referenceName.equals("referenceToFlowPropertyDataSet"))
            return GlobalReferenceTypeValues.fromValue("flow property data set");
        else if (referenceName.equals("referenceToIncludedMethods"))
            return GlobalReferenceTypeValues.fromValue("LCIA method data set");
        else if (referenceName.equals("referenceToIncludedNormalisationDataSets"))
            return GlobalReferenceTypeValues.fromValue("source data set");
        else if (referenceName.equals("referenceToIncludedProcesses"))
            return GlobalReferenceTypeValues.fromValue("process data set");
        else if (referenceName.equals("referenceToIncludedSubMethods"))
            return GlobalReferenceTypeValues.fromValue("LCIA method data set");
        else if (referenceName.equals("referenceToIncludedWeightingDataSets"))
            return GlobalReferenceTypeValues.fromValue("source data set");
        else if (referenceName.equals("referenceToLCAMethodDetails"))
            return GlobalReferenceTypeValues.fromValue("source data set");
        else if (referenceName.equals("referenceToLCIAMethodDataSet"))
            return GlobalReferenceTypeValues.fromValue("LCIA method data set");
        else if (referenceName.equals("referenceToLCIAMethodFlowDiagrammOrPicture"))
            return GlobalReferenceTypeValues.fromValue("source data set");
        else if (referenceName.equals("referenceToLogo"))
            return GlobalReferenceTypeValues.fromValue("source data set");
        else if (referenceName.equals("referenceToMethodologyFlowChart"))
            return GlobalReferenceTypeValues.fromValue("source data set");
        else if (referenceName.equals("referenceToModelSource"))
            return GlobalReferenceTypeValues.fromValue("source data set");
        else if (referenceName.equals("referenceToNameOfReviewerAndInstitution"))
            return GlobalReferenceTypeValues.fromValue("contact data set");
        else if (referenceName.equals("referenceToOwnershipOfDataSet"))
            return GlobalReferenceTypeValues.fromValue("contact data set");
        else if (referenceName.equals("referenceToPersonOrEntityEnteringTheData"))
            return GlobalReferenceTypeValues.fromValue("contact data set");
        else if (referenceName.equals("referenceToPersonOrEntityGeneratingTheDataSet"))
            return GlobalReferenceTypeValues.fromValue("contact data set");
        else if (referenceName.equals("referenceToPrecedingDataSetVersion"))
            return getType(type);
        else if (referenceName.equals("referenceQuantity"))
            return GlobalReferenceTypeValues.fromValue("flow property data set");
        else if (referenceName.equals("referenceToRawDataDocumentation"))
            return GlobalReferenceTypeValues.fromValue("source data set");
        else if (referenceName.equals("referenceToReferenceUnitGroup"))
            return GlobalReferenceTypeValues.fromValue("unit group data set");
        else if (referenceName.equals("referenceToRegistrationAuthority"))
            return GlobalReferenceTypeValues.fromValue("contact data set");
        else if (referenceName.equals("referenceToSource"))
            return GlobalReferenceTypeValues.fromValue("source data set");
        else if (referenceName.equals("referenceToSupportedImpactAssessmentMethods"))
            return GlobalReferenceTypeValues.fromValue("LCIA method data set");
        else if (referenceName.equals("referenceToTechnicalSpecification"))
            return GlobalReferenceTypeValues.fromValue("source data set");
        else if (referenceName.equals("referenceToTechnologyFlowDiagrammOrPicture"))
            return GlobalReferenceTypeValues.fromValue("source data set");
        else if (referenceName.equals("referenceToTechnologyPictogramme"))
            return GlobalReferenceTypeValues.fromValue("source data set");
        else if (referenceName.equals("referenceToUnchangedRepublication"))
            return GlobalReferenceTypeValues.fromValue("source data set");
        else if (referenceName.equals("referenceToUsableNormalisationDataSets"))
            return GlobalReferenceTypeValues.fromValue("source data set");
        else if (referenceName.equals("referenceToUsableWeightingDataSets"))
            return GlobalReferenceTypeValues.fromValue("source data set");
        else return null;
    }
        
    public static GlobalReferenceTypeValues getType(Class<?> clazz, ILCDTypes type) {
        String className = clazz.getName();
        className = className.substring(0, 1).toUpperCase() + className.substring(1, className.length());
        return getType(className, type);
    }
            
    public static GlobalReferenceTypeValues getType(ILCDTypes type) {
        switch (type) {
            case CONTACT: return GlobalReferenceTypeValues.fromValue("contact data set");
            case FLOW: return GlobalReferenceTypeValues.fromValue("flow data set");
            case FLOWPROPERTY: return GlobalReferenceTypeValues.fromValue("flow property data set");
            case LCIAMETHOD: return GlobalReferenceTypeValues.fromValue("LCIA method data set");
            case PROCESS: return GlobalReferenceTypeValues.fromValue("process data set");
            case SOURCE: return GlobalReferenceTypeValues.fromValue("source data set");
            case UNITGROUP: return GlobalReferenceTypeValues.fromValue("unit group data set");
            default: return null;
        }    
    }
}
        