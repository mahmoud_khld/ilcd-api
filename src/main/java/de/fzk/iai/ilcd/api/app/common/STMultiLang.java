package de.fzk.iai.ilcd.api.app.common;

import de.fzk.iai.ilcd.api.Configuration;

public class STMultiLang extends de.fzk.iai.ilcd.api.binding.generated.common.STMultiLang {

	public STMultiLang() {
	}

	public STMultiLang(String value, String lang) {
		this.value = value;
		this.lang = lang;
	}

	public STMultiLang(String value) {
		this.value = value;
		this.lang = Configuration.getInstance().getLanguage();
	}

}
