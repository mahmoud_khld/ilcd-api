package de.fzk.iai.ilcd.api.util;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.log4j.Logger;

public class WhiteSpaceFilterInputStream extends FilterInputStream {

	protected final static Logger log = org.apache.log4j.Logger.getLogger(WhiteSpaceFilterInputStream.class);

	public WhiteSpaceFilterInputStream(InputStream in) {
		super(in);
	}

	protected StringBuffer buf = new StringBuffer();

	private boolean endOfStream = false;

	// returns the contents of the buffer char by char
	protected int getNextFromBuffer() {
		int c = buf.charAt(0);
		buf.reverse();
		buf.setLength(buf.length() - 1);
		buf.reverse();
		return c;
	}

	public int read() throws IOException {
		int c;

		// log.debug("avail " + in.available());

		if (buf.length() == 0) {
			// read next char
			c = in.read();

			// skip whitespace (convert to single space)
			if (c == 9 || c == 32 || c == '\n') {
				while ((c == 9 || c == 32 || c == '\n') && in.available() > 0) {
					c = in.read();
				}
				buf.append((char) 32);
				if (in.available() > 0)
					buf.append((char) c);
				c = getNextFromBuffer();
			}

			// look for &#160; entities and replace w/line break
			if (c == 38) {
				// it's an &, look what's coming:
				int d = in.read();
				int e = in.read();
				int f = in.read();
				int g = in.read();
				int h = in.read();

				if (d == 35 && e == 49 && f == 54 && g == 48 && h == 59) {
					// it's &#160;
					buf.append("\n");
				} else {
					buf.append((char) c);
					buf.append((char) d);
					buf.append((char) e);
					buf.append((char) f);
					buf.append((char) g);
					buf.append((char) h);
				}
				c = getNextFromBuffer();
			}

		} else {
			c = getNextFromBuffer();
		}

		// log.debug(c + " - " + ((char) c) + "(" + in.available() + ")");

		return c;

	}

	public int read(byte[] b, int offset, int length) throws IOException {

		if (endOfStream)
			return -1;
		int numRead = 0;

		for (int i = offset; i < offset + length; i++) {
			int temp = this.read();
			if (temp == -1) {
				this.endOfStream = true;
				break;
			}
			b[i] = (byte) temp;
			numRead++;
		}
		return numRead;
	}

}
