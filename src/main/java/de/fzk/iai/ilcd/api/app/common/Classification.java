package de.fzk.iai.ilcd.api.app.common;

import java.util.ArrayList;
import java.util.List;

import de.fzk.iai.ilcd.api.binding.generated.common.ClassType;

public class Classification extends de.fzk.iai.ilcd.api.binding.generated.common.ClassificationType {

	/**
	 * 
	 */
	public Classification() {
	}

	/**
	 * Convenience constructor
	 * 
	 * @param clazz
	 */
	public Classification(Class clazz) {
		this.add(clazz);
	}

	/**
	 * Convenience method to add a de.fzk.iai.ilcd.api.app.common.Class without
	 * calling the getClazz() method
	 * 
	 * @param clazz
	 */
	public void add(Class clazz) {
		this.getClazz().add(clazz);
	}

	/**
	 * Remove the category that matches the level and value of the one passed as
	 * parameter
	 * 
	 * @param clazz
	 */
	public void remove(Class clazz) {
		int removalCandidateIndex = -1;

		for (ClassType classType : this.getClazz()) {
			if (classType.getLevel().intValue() == clazz.getLevel().intValue() && classType.getValue().equals(clazz.getValue()))
				removalCandidateIndex = this.getClazz().indexOf(classType);
		}

		if (removalCandidateIndex != -1)
			this.getClazz().remove(removalCandidateIndex);
	}

	/**
	 * Remove all categories whose level is equal or greater than the parameter
	 * 
	 * @param level
	 */
	public void removeAll(int level) {
		List<ClassType> removalCandidates = new ArrayList<ClassType>();

		for (ClassType classType : this.getClazz()) {
			if (classType.getLevel().intValue() >= level)
				removalCandidates.add(classType);
		}

		if (!removalCandidates.isEmpty())
			this.getClazz().removeAll(removalCandidates);
	}

	/**
	 * Convenience method to get the number of present
	 * de.fzk.iai.ilcd.api.app.common.Class elements without calling the
	 * getClazz() method
	 * 
	 * @return the size
	 */
	public int size() {
		return this.getClazz().size();
	}
}
