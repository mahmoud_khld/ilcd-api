package de.fzk.iai.ilcd.api.app.process;

import javax.xml.namespace.QName;

public class VariableParameter extends de.fzk.iai.ilcd.api.binding.generated.process.VariableParameterType {

	public static final QName TYPE_ATTRIBUTE = new QName("type");

	public static final String PARAMETER = "parameter";

	public void setType(String type) {
		if (type == null) {
			this.getOtherAttributes().remove(TYPE_ATTRIBUTE);
		} else {
			this.getOtherAttributes().put(TYPE_ATTRIBUTE, type);
		}
	}

	public String getType() {
		return this.getOtherAttributes().get(TYPE_ATTRIBUTE);
	}

	public boolean isParameter() {
		if (this.getOtherAttributes().get(TYPE_ATTRIBUTE) != null)
			return this.getOtherAttributes().get(TYPE_ATTRIBUTE).equalsIgnoreCase(PARAMETER);
		else
			return false;
	}

	public void setIsParameter(boolean bool) {
		if (bool)
			this.setType(PARAMETER);
		else
			this.setType(null);
	}

}
