package de.fzk.iai.ilcd.api.util;

import org.apache.log4j.Logger;

public class PathUtil {

	private static Logger logger = Logger.getLogger(PathUtil.class);

	public static final String NAME_SEPARATOR = "-";

	public static final String INDEX_SEPARATOR = "_";

	public static final String SUBFIELD_SEPARATOR = "+";

	public static String getName(String path) {
		logger.debug("getting canonical name from id " + path);
		if (hasIndex(path)) {
			int sep1 = path.lastIndexOf(INDEX_SEPARATOR);
			return path.substring(0, sep1);
		} else {
			return path;
		}
	}

	public static Integer getIndex(String path) {
		if (hasIndex(path)) {
			int sep1 = path.lastIndexOf(INDEX_SEPARATOR);
			return Integer.parseInt(path.substring(sep1 + 1, path.length()));
		} else {
			return null;
		}
	}

	public static boolean hasIndex(String path) {
		if (!path.contains(INDEX_SEPARATOR))
			return false;
		int position = path.lastIndexOf(INDEX_SEPARATOR);
		boolean namesAfterLastIndex = path.substring(position).contains(NAME_SEPARATOR);
		return (!namesAfterLastIndex);
	}

	public static String getZeroInitializedPath(String path) {
		return path.replaceAll("_[0-9]*", "_1");
	}

	public static String firstCharUp(String arg) {
		return arg.substring(0, 1).toUpperCase() + arg.substring(1, arg.length());
	}

	// lower first character, but only if the String does not start with an
	// all-caps substring (i.e., LCIMethod will stay the same)
	public static String firstCharDown(String arg) {
		if (!arg.substring(1, 2).equals(arg.substring(1, 2).toUpperCase()))
			return arg.substring(0, 1).toLowerCase() + arg.substring(1, arg.length());
		else
			return arg;
	}

	public static String xPathToOliPath(String xpath) {
		String result = xpath.replaceAll("/", NAME_SEPARATOR);
		result = result.replaceAll("\\[", INDEX_SEPARATOR);
		result = result.replaceAll("\\]", "");
		return result;
	}

	public static String oliPathToXPath(String olipath) {
		String result = olipath.replaceAll(NAME_SEPARATOR, "/");
		result = result.replaceAll(INDEX_SEPARATOR, "[");
		result = result.replaceAll("(?<=\\[[0-9]{0,100000})", "]");
		return result;
	}

	public static String escapeRegExPattern(String arg) {
		return arg.replaceAll("\\$", "\\\\\\$").replaceAll("\\[", "\\\\[").replaceAll("\\]", "\\\\]");
	}
}
