package de.fzk.iai.ilcd.api.util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;

import de.fzk.iai.ilcd.api.Configuration;
import de.fzk.iai.ilcd.api.app.common.GlobalReference;
import de.fzk.iai.ilcd.api.binding.generated.common.FTMultiLang;
import de.fzk.iai.ilcd.api.binding.generated.common.GlobalReferenceType;
import de.fzk.iai.ilcd.api.binding.generated.common.GlobalReferenceTypeValues;
import de.fzk.iai.ilcd.api.binding.generated.common.ObjectFactory;
import de.fzk.iai.ilcd.api.binding.generated.common.STMultiLang;
import de.fzk.iai.ilcd.api.binding.generated.common.StringMultiLang;
import de.fzk.iai.ilcd.api.binding.helper.MetaObjectFactory;
import de.fzk.iai.ilcd.api.dataset.DataSet;
import de.fzk.iai.ilcd.api.dataset.ILCDTypes;
import de.fzk.iai.ilcd.api.mapper.GlobalReferenceDatasetTypeMapper;

public class TreeWalker {
	protected final static Logger log = org.apache.log4j.Logger.getLogger(TreeWalker.class);

	private StringTokenizer tokenizer;

	private Object objectFactory;

	private List<String> stack;

	private List<String> result = new ArrayList<String>();

	private ILCDTypes type = null;

	public static int doWalk(String id, DataSet root, Object target) {

		log.debug("treewalker here");

		if (target != null) {
			log.debug("not null, nothing to do for " + target.getClass().getName());
			return 0;
		}

		log.debug(root.getClass().getName());
		log.debug(id);

		String rootElement = id.substring(0, id.indexOf("-"));
		log.debug(rootElement);

		ILCDTypes type = ILCDTypes.determineType(rootElement);

		log.debug(type.getValue());

		TreeWalker walker = new TreeWalker(id, type);
		try {
			walker.initialize(root);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}

	public TreeWalker(String destPath, ILCDTypes type) {

		this.objectFactory = MetaObjectFactory.getInstance().getObjectFactory(type);

		this.tokenizer = new StringTokenizer(destPath, PathUtil.NAME_SEPARATOR);
		if (this.tokenizer.hasMoreTokens()) {
			this.tokenizer.nextToken();
		} else {
			throw new IllegalArgumentException("Path is empty");
		}
		this.type = type;
	}

	public TreeWalker(DataSet dataset) {

		this.objectFactory = MetaObjectFactory.getInstance().getObjectFactory(dataset.getDatasetType());

		this.stack = new ArrayList<String>();
	}

	@SuppressWarnings("unchecked")
	public void walk(Object root) {

		try {

			if (root == null)
				return;

			Class clazz = root.getClass();

			// do not traverse the following types
			if (clazz.getSimpleName().equals("StringMultiLang") || clazz.getSimpleName().equals("STMultiLang") || clazz.getSimpleName().equals("FTMultiLang")
					|| clazz.getSimpleName().equals("GlobalReference") || clazz.getSimpleName().equals("GlobalReferenceType"))
				return;

			if (log.isDebugEnabled()) {
				log.debug("current object is a " + clazz.getName());
				log.debug("current stack is    " + stack);
			}

			String lastStackElement = (stack.isEmpty() ? "" : stack.get(stack.size() - 1));
			if (lastStackElement.contains("_")) {
				lastStackElement = lastStackElement.substring(0, lastStackElement.lastIndexOf("_"));
			}

			boolean add = true;
			if (lastStackElement.equalsIgnoreCase(clazz.getSimpleName())) {
				log.debug("NO!");
				add = false;
			}
			if (add) {
				if (log.isDebugEnabled())
					log.debug("adding " + clazz.getSimpleName() + " to stack (" + lastStackElement + ")");
				stack.add(PathUtil.firstCharDown(clazz.getSimpleName()));
			}
			Method[] methods = clazz.getMethods();

			// iterate over getter methods
			for (Method m : methods) {
				if (m.getName().startsWith("get") && m.getParameterTypes().length == 0) {
					Object child = m.invoke(root, (Object[]) null);

					if (child == null) {
						if (log.isDebugEnabled())
							log.debug("last item on stack is " + stack.get(stack.size() - 1));
						continue;
					}

					if (!(child.getClass().getPackage().getName().startsWith("de.fzk.iai.ilcd.api.app") || child instanceof Collection)) {
						if (log.isDebugEnabled())
							log.debug("last item on stack is " + stack.get(stack.size() - 1));
						continue;
					}

					if (log.isDebugEnabled())
						log.debug("found child " + child.getClass().getName() + " " + m.getName());

					if (child instanceof Collection) {

						Collection<Object> coll = (Collection<Object>) child;

						ParameterizedType pm = (ParameterizedType) m.getGenericReturnType();
						Type[] t = pm.getActualTypeArguments();
						Class tclass = (Class) t[0];
						String genType = tclass.getSimpleName();

						if (genType.equals("StringMultiLang") || genType.equals("STMultiLang") || genType.equals("FTMultiLang")) {
							continue;
						}

						if (coll.isEmpty()) {
							if (log.isDebugEnabled())
								log.debug("last item on stack is " + stack.get(stack.size() - 1));
							continue;
						}

						if (tclass == StringMultiLang.class || tclass == STMultiLang.class || tclass == FTMultiLang.class || tclass == GlobalReference.class) {
							continue;
						}

						String methodName = m.getName();

						if (log.isDebugEnabled()) {
							log.debug("found a collection: " + child.getClass().getName());
							log.debug(" param type is " + genType);
						}

						Iterator it = coll.iterator();
						int i = 1;
						while (it.hasNext()) {
							Object collMember = it.next();
							if (log.isDebugEnabled())
								log.debug("adding " + methodName.substring(3) + "_" + i + " to stack");
							stack.add(PathUtil.firstCharDown(methodName.substring(3)) + "_" + i);
							if (log.isDebugEnabled())
								log.debug("DO ACTION HERE ---> " + stackToPath(stack));

							// the first widget set is already there, call
							// insertItem only for additional ones
							if (i > 1)
								result.add(stackToPath(stack));
							walk(collMember);

							if (log.isDebugEnabled())
								log.debug("removing " + stack.get(stack.size() - 1) + " from stack (" + methodName.substring(3) + ")");
							stack.remove(stack.size() - 1);
							i++;
						}
						//

					} else {

						walk(child);

					}
				}

			}

			if (log.isDebugEnabled())
				log.debug(stack.size());
			lastStackElement = (stack.size() > 1 ? stack.get(stack.size() - 2) : "");
			if (lastStackElement.contains("#")) {
				lastStackElement = lastStackElement.substring(0, lastStackElement.lastIndexOf("_"));
			}

			if (lastStackElement.equals(stack.get(stack.size() - 1)))
				log.debug("NO!");

			if (add) {
				if (log.isDebugEnabled())
					log.debug("removing " + stack.get(stack.size() - 1) + " from stack");
				stack.remove(stack.size() - 1);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private String stackToPath(List<String> stack) {
		StringBuffer buf = new StringBuffer();
		Iterator<String> it = stack.iterator();
		while (it.hasNext()) {
			String s = (String) it.next();
			buf.append(s);
			if (it.hasNext())
				buf.append("-");
		}
		return buf.toString();
	}

	/**
	 * Algorithm: - n�chstes Token holen - auf root-Objekt: child = get[Token] -
	 * wenn liste, d.h. index!=null when child == null child = neue Liste
	 * root.setChild(child)
	 * 
	 * solange Liste.size < index Liste.add(new Child)
	 * 
	 * child = Liste.get(index)
	 * 
	 * sonst
	 * 
	 * wenn child == null erzeuge child root(set[Child]) - initialize(child)
	 * 
	 * @param root
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	private void initialize(Object root) throws Exception {

		log.debug("entering initialize()");

		while (this.tokenizer.hasMoreTokens()) {

			// get next token
			String currentToken = this.tokenizer.nextToken();

			String currentElement = PathUtil.getName(currentToken);
			Integer currentIndex = PathUtil.getIndex(currentToken);
			String currentElementU = PathUtil.firstCharUp(currentElement);

			log.debug("processing token " + currentElement + " (" + this.tokenizer.countTokens() + " left)");
			log.debug("current index is " + currentIndex);

			// skip root element since it's already instantiated
			if (this.type.equals(ILCDTypes.determineType(currentElementU))) {
				log.debug(" skipping top-level path element " + currentElement);
				continue;
			}

			Object child = null;

			// call get[token] method on root
			log.debug("calling get" + currentElementU + " on " + root.getClass().getName());
			Method getMethod = root.getClass().getMethod("get" + currentElementU, (Class[]) null);
			Type returnType = getMethod.getGenericReturnType();
			log.debug("method's return type is         " + (returnType instanceof Class ? ((Class) returnType).getName() : ""));
			child = getMethod.invoke(root, (Object[]) null);

			boolean isList = (currentIndex != null);

			log.debug("current element holds a list: " + isList);

			if (!isList) {
				// child is no list:
				// create child
				// call root.setChild(child)
				log.debug("child is null: " + (child == null));

				if (child == null) {

					log.debug(currentElementU + " " + ((Class) returnType).getSimpleName());

					child = createObject(currentElementU, returnType);

					log.debug("calling set" + currentElementU + " on " + root.getClass().getName());
					Method setMethod = root.getClass().getMethod("set" + currentElementU, (Class) returnType);
					setMethod.invoke(root, child);
				}
			} else {
				// child is a list
				if (child == null) {
					// create a new list
					child = new ArrayList<Object>();
					log.debug("calling set" + currentElementU + " on " + root.getClass().getName());
					Method setMethod = root.getClass().getMethod("set" + currentElementU, (Class) returnType);
					setMethod.invoke(root, child);
				}

				// analyze what type the list members are supposed to be
				ParameterizedType pm = (ParameterizedType) returnType;
				Type[] t = pm.getActualTypeArguments();
				Class<?> tclass = (Class<?>) t[0];
				String genType = tclass.getSimpleName();

				log.debug("this lists hold members of type " + genType);

				List<Object> list = (List<Object>) child;
				// ensure that current element is not null
				// fill up list until it has currentIndex members
				if (list.size() == currentIndex && list.get(currentIndex - 1) == null) {
					Object listMember = createObject(currentElementU, tclass);
					setDataSetInternalId(listMember, list);
					list.set(currentIndex - 1, listMember);
				}
				while (list.size() < currentIndex) {
					Object listMember = createObject(currentElementU, tclass);
					setDataSetInternalId(listMember, list);
					list.add(listMember);
				}

				log.debug(list);

				child = list.get(currentIndex - 1);
			}

			// continue with next child
			initialize(child);
		}
	}

	protected void setDataSetInternalId(Object obj, List<Object> parent) {
		log.debug("entering setDataSetInternalId");
		try {
			Method setDataSetInternalId = obj.getClass().getMethod("setDataSetInternalID", BigInteger.class);

			int newId = getHighestInternalId(parent);

			log.debug("setting dataSetInternalId to " + newId);

			setDataSetInternalId.invoke(obj, BigInteger.valueOf(newId));

			log.debug("success");

		} catch (SecurityException e) {
			log.debug(e);
		} catch (NoSuchMethodException e) {
			log.debug(e);
		} catch (IllegalArgumentException e) {
			log.debug(e);
		} catch (IllegalAccessException e) {
			log.debug(e);
		} catch (InvocationTargetException e) {
			log.debug(e);
		}
	}

	protected int getHighestInternalId(List<Object> list) {

		log.debug("getting highest internalId for " + list.toString());

		int id = 0;

		for (Object member : list) {
			try {
				Method getDataSetInternalId = member.getClass().getMethod("getDataSetInternalID", (Class[]) null);
				id = Math.max(id, ((BigInteger) getDataSetInternalId.invoke(member, (Object[]) null)).intValue() + 1);
			} catch (NullPointerException e) {
				log.debug(e);
			} catch (IllegalArgumentException e) {
				log.debug(e);
			} catch (IllegalAccessException e) {
				log.debug(e);
			} catch (InvocationTargetException e) {
				log.debug(e);
			} catch (SecurityException e) {
				log.debug(e);
			} catch (NoSuchMethodException e) {
				log.debug(e);
			}
		}

		log.debug("highest id is " + id);

		return id;
	}

	/**
	 * This method tries to create an instance of an application-level binding
	 * class (from the de.fzk.iai.ilcd.api.app package). It will try to detect
	 * the type that needs to be created and if needed use an object factory.
	 * Returns null if the object couldn't be created
	 * 
	 * @param simpleName
	 *            the simple name of the class to create, without the package
	 * @param returnType
	 *            the return type of the corresponding getter method, used to
	 *            detect types like GlobalReference
	 * @return the created object or null if it couldn't be created
	 */
	protected Object createObject(String simpleName, Type returnType) {
		log.debug("Trying to create object " + simpleName);
		// sort out special cases where no factory method is
		// available
		if (returnType == GlobalReferenceType.class) {
			GlobalReferenceType result = new GlobalReferenceType();
			GlobalReferenceTypeValues refType = GlobalReferenceDatasetTypeMapper.getType(PathUtil.firstCharDown(simpleName), this.type);
			result.setType(refType);
			return result;
		} else if (returnType == String.class) {
			return new String();
		} else if (((Class<?>) returnType).isEnum()) {
			log.debug("it's an enum");
			return null;
		} else {
			// all other cases: use object factory
			// if the simple name does not match the return type, use the latter
			// instead
			if (!((Class<?>) returnType).getSimpleName().endsWith(simpleName + "Type")
					&& ((Class<?>) returnType).getPackage().getName().startsWith(de.fzk.iai.ilcd.api.Constants.class.getPackage().getName())) {
				return createObjectUsingFactory(((Class<?>) returnType).getSimpleName());
			} else
				return createObjectUsingFactory(simpleName);
		}

	}

	/**
	 * This method tries to create an instance of an application-level binding
	 * class (from the de.fzk.iai.ilcd.api.app package) using the current object
	 * factory. If it doesn't succeed it tries the common object factory as
	 * well. Returns null if the object couldn't be created
	 * 
	 * @param simpleName
	 *            the simple name of the class to create, without the package
	 * @return the created object or null if it couldn't be created
	 */
	protected Object createObjectUsingFactory(String simpleName) {

		Object factory = this.objectFactory;
		Method factoryMethod = null;
		Object result = null;

		String createMethodName = "create" + simpleName + (simpleName.endsWith("Type") ? "" : "Type");

		// special treatment for review scope and method
		// for Scope -> createReviewTypeScope
		// for Method -> createReviewTypeScopeMethod
		if (simpleName.equals("Scope") || simpleName.equals("Method")) {
			createMethodName = "createReviewType" + (simpleName.equals("Method") ? "Scope" : "") + simpleName;
		}

		try {
			// try the default factory
			log.debug("trying factory method " + createMethodName);
			factoryMethod = factory.getClass().getMethod(createMethodName, (Class[]) null);
			result = factoryMethod.invoke(factory, (Object[]) null);
			log.debug("success");
		} catch (Exception e) {
			log.debug("NO success, trying common factory");
			// try common factory
			try {
				Object commonFactory = new ObjectFactory();
				factoryMethod = commonFactory.getClass().getMethod(createMethodName, (Class[]) null);
				result = factoryMethod.invoke(commonFactory, (Object[]) null);
				log.debug("success");
			} catch (Exception e1) {
				log.warn("NO success: " + e1.getMessage());
			}
		}
		return result;
	}

	public Object getValue(Object root) {

		log.debug("calling getValue");

		Object obj = root;
		String currentToken;
		String currentElement;
		Integer currentIndex;

		try {

			while (this.tokenizer.hasMoreTokens()) {

				currentToken = this.tokenizer.nextToken();

				currentElement = PathUtil.getName(currentToken);
				currentIndex = PathUtil.getIndex(currentToken);

				log.debug("processing token " + currentElement + " (" + this.tokenizer.countTokens() + " left)");

				if (root.getClass().getSimpleName().equals(PathUtil.firstCharUp(currentElement))) {
					log.debug("   skipping top-level path element " + currentElement);
					continue;
				}

				if (this.tokenizer.countTokens() == 0) {
					// we are there
					log.debug("  reached last path element.");

					Method m = obj.getClass().getMethod("get" + PathUtil.firstCharUp(currentElement), (Class[]) null);
					log.debug("  calling " + m.getName() + " on " + obj.getClass().getSimpleName());

					obj = m.invoke(obj, (Object[]) null);

					// decide if it's a List (of String, MultiLang, GlobalRef)
					// or a String or a GlobalRef or a BigDecimal or BigInteger
					// or Double
					if (obj instanceof List<?>) {

						log.debug("   it's a list");

						ParameterizedType pm = (ParameterizedType) m.getGenericReturnType();
						Type[] t = pm.getActualTypeArguments();
						Class<?> tclass = (Class<?>) t[0];
						String genType = tclass.getSimpleName();
						log.debug("    param type is " + genType);

						// process list contents
						if (genType.equals("StringMultiLang") || genType.equals("STMultiLang") || genType.equals("FTMultiLang"))
							return getEnValue((List<?>) obj);
						else {
							log.debug(" current index is " + currentIndex);
							log.debug(" list has " + ((List<?>) obj).size() + " items");
							return (List<?>) obj;
						}

					} else if (obj instanceof Enum<?>) {
						log.debug("  it's an Enumeration");
						Enum<?> en = (Enum<?>) obj;
						log.debug(en.getClass().getSimpleName());
						Method valueMethod = en.getClass().getMethod("value", (Class[]) null);
						String value = (String) valueMethod.invoke(en, (Object[]) null);

						log.debug("   enum value is " + value);
						return en;
					}

					log.debug("return type is " + m.getReturnType());
					return obj;

				} else {
					if (obj == null) {
						// if it's null, abort
						log.debug("obj is null, returning null");
						return null;

					} else {

						// if its an object type, call getter method
						Method m = obj.getClass().getMethod("get" + PathUtil.firstCharUp(currentElement), (Class[]) null);
						log.debug("  calling " + m.getName() + " on " + obj.getClass().getSimpleName());
						obj = m.invoke(obj, (Object[]) null);

						if (obj instanceof List<?>) {
							// if its a list, get list item with currentIndex
							ParameterizedType pm = (ParameterizedType) m.getGenericReturnType();
							Type[] t = pm.getActualTypeArguments();
							Class<?> tclass = (Class<?>) t[0];

							log.debug(obj.getClass().getSimpleName() + " is a list<" + tclass.getSimpleName() + ">, trying to get item #" + currentIndex);
							if (obj != null)
								log.debug("list has total of " + ((List<?>) obj).size() + " items");
							else
								log.debug("list is null!");
							try {
								obj = ((List<?>) obj).get(currentIndex - 1);
							} catch (IndexOutOfBoundsException e) {
								log.debug("item not found, returning null. " + e.getMessage());
								return null;
							}
						}
					}
				}

			}

		} catch (Exception e) {
			log.error(e);
		}
		return null;
	}

	/**
	 * Returns the first value holding an appropriate language attribute from a
	 * List of StringMultiLang, STMultiLang or FTMultiLang items
	 * 
	 * @param list
	 *            the list
	 * @return the first MultiLang item's value that has a lang attribute
	 *         matching default lang; if none is present, null
	 */
	private String getEnValue(List<?> list) {
		return MultiLangUtil.getString(list, Configuration.getInstance().getLanguage());
	}

	public List<String> getResult() {
		return result;
	}

}
