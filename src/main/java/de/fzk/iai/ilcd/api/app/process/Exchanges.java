package de.fzk.iai.ilcd.api.app.process;

import java.math.BigInteger;

import de.fzk.iai.ilcd.api.binding.generated.process.ExchangeType;

public class Exchanges extends de.fzk.iai.ilcd.api.binding.generated.process.ExchangesType {

	public ExchangeType getExchangeById(BigInteger dataSetInternalId) {
		if (dataSetInternalId == null)
			return null;
		for (ExchangeType e : this.exchange) {
			try {
				if (dataSetInternalId.equals(e.getDataSetInternalID())) {
					return e;
				}
			} catch (NullPointerException e1) {
			}
		}
		return null;
	}
}
