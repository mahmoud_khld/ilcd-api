package de.fzk.iai.ilcd.api.dataset;

public class ILCDXPathConstants {

	static final String TOP_CATEGORY_XPATH = "/*/*/*[local-name()='dataSetInformation']/*[local-name()='classificationInformation']/common:classification/common:class[@level=0]";

	static final String SUB_CATEGORY_XPATH = "/*/*/*[local-name()='dataSetInformation']/*[local-name()='classificationInformation']/common:classification/common:class[@level=1]";

	static final String ELEMENTARY_FLOW_TOP_CATEGORY_XPATH = "/flow:flowDataSet/flow:flowInformation/flow:dataSetInformation/flow:classificationInformation/common:elementaryFlowCategorization/common:category[@level=0]";

	static final String ELEMENTARY_FLOW_SUB_CATEGORY_XPATH = "/flow:flowDataSet/flow:flowInformation/flow:dataSetInformation/flow:classificationInformation/common:elementaryFlowCategorization/common:category[@level=1]";

	static final String ELEMENTARY_FLOW_SUB_CATEGORY2_XPATH = "/flow:flowDataSet/flow:flowInformation/flow:dataSetInformation/flow:classificationInformation/common:elementaryFlowCategorization/common:category[@level=2]";

	static final String PROCESS_NAME_BASE_XPATH = "/process:processDataSet/process:processInformation/process:dataSetInformation/process:name/process:baseName[@xml:lang='en']";

	static final String PROCESS_NAME_TREATMENT_XPATH = "/process:processDataSet/process:processInformation/process:dataSetInformation/process:name/process:treatmentStandardsRoutes[@xml:lang='en']";

	static final String PROCESS_NAME_MIX_XPATH = "/process:processDataSet/process:processInformation/process:dataSetInformation/process:name/process:mixAndLocationTypes[@xml:lang='en']";

	static final String PROCESS_NAME_FUNCTIONALUNIT_XPATH = "/process:processDataSet/process:processInformation/process:dataSetInformation/process:name/process:functionalUnitFlowProperties[@xml:lang='en']";

	static final String PROCESS_LOCATION_XPATH = "/process:processDataSet/process:processInformation/process:geography/process:locationOfOperationSupplyOrProduction/@location";

	static final String PROCESS_REFERENCE_YEAR_XPATH = "/process:processDataSet/process:processInformation/process:time/common:referenceYear";

	static final String PROCESS_TYPE_XPATH = "/process:processDataSet/process:modellingAndValidation/process:LCIMethodAndAllocation/process:typeOfDataSet";

	static final String FLOW_NAME_BASE_XPATH = "/flow:flowDataSet/flow:flowInformation/flow:dataSetInformation/flow:name/flow:baseName[@xml:lang='en']";

	static final String FLOW_NAME_TREATMENT_XPATH = "/flow:flowDataSet/flow:flowInformation/flow:dataSetInformation/flow:name/flow:treatmentStandardsRoutes[@xml:lang='en']";

	static final String FLOW_NAME_MIX_XPATH = "/flow:flowDataSet/flow:flowInformation/flow:dataSetInformation/flow:name/flow:mixAndLocationTypes[@xml:lang='en']";

	static final String FLOW_NAME_FUNCTIONALUNIT_XPATH = "/flow:flowDataSet/flow:flowInformation/flow:dataSetInformation/flow:name/flow:functionalUnitFlowProperties[@xml:lang='en']";

	static final String FLOW_LOCATION_XPATH = "/flow:flowDataSet/flow:flowInformation/flow:geography/flow:locationOfSupply[@xml:lang='en']";

	static final String FLOW_TYPE_XPATH = "/flow:flowDataSet/flow:modellingAndValidation/flow:LCIMethod/flow:typeOfDataSet";

	static final String FLOWPROPERTY_NAME_XPATH = "/flowproperty:flowPropertyDataSet/flowproperty:flowPropertiesInformation/flowproperty:dataSetInformation/common:name[@xml:lang='en']";

	static final String UNITGROUP_NAME_XPATH = "/unitgroup:unitGroupDataSet/unitgroup:unitGroupInformation/unitgroup:dataSetInformation/common:name[@xml:lang='en']";

	static final String LCIAMETHOD_NAME_XPATH = "/lciamethod:LCIAMethodDataSet/lciamethod:LCIAMethodInformation/lciamethod:dataSetInformation/common:name[@xml:lang='en']";

	static final String SOURCE_NAME_XPATH = "/source:sourceDataSet/source:sourceInformation/source:dataSetInformation/common:shortName[@xml:lang='en']";

	static final String CONTACT_NAME_XPATH = "/contact:contactDataSet/contact:contactInformation/contact:dataSetInformation/common:name[@xml:lang='en']";

}
