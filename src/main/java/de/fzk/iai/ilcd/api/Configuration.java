package de.fzk.iai.ilcd.api;

public class Configuration {

	private static Configuration _instance = null;

	private String language = Constants.DEFAULT_LANGUAGE;

	public synchronized static Configuration getInstance() {
		if (_instance == null)
			_instance = new Configuration();
		return _instance;
	}

	private Configuration() {

	}

	/**
	 * @return the language
	 */
	public String getLanguage() {
		return language;
	}

	/**
	 * @param language
	 *            the language to set
	 */
	public void setLanguage(String language) {
		this.language = language;
	}

}
