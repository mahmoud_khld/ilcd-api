package de.fzk.iai.ilcd.api.validation;

import javax.xml.bind.ValidationEvent;
import javax.xml.bind.util.ValidationEventCollector;

public class MyValidationEventCollector extends ValidationEventCollector {

	public MyValidationEventCollector() {
		super();
	}

	@Override
	public boolean handleEvent(ValidationEvent arg0) {
		super.handleEvent(arg0);
		return true;
	}
}
