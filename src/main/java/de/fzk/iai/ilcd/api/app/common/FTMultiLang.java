package de.fzk.iai.ilcd.api.app.common;

import de.fzk.iai.ilcd.api.Configuration;

public class FTMultiLang extends de.fzk.iai.ilcd.api.binding.generated.common.FTMultiLang {

	public FTMultiLang() {
	}

	public FTMultiLang(String value, String lang) {
		this.value = value;
		this.lang = lang;
	}

	public FTMultiLang(String value) {
		this.value = value;
		this.lang = Configuration.getInstance().getLanguage();
	}
}
