/**
 * 
 */
package de.fzk.iai.ilcd.api.validation;

import javax.swing.DefaultListModel;
import javax.xml.bind.ValidationEvent;
import javax.xml.bind.helpers.DefaultValidationEventHandler;

import org.apache.log4j.Logger;

public class ValidationEventHandler extends DefaultValidationEventHandler {

	protected static final Logger log = org.apache.log4j.Logger.getLogger(ValidationEventHandler.class);

	private DefaultListModel model = null;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * javax.xml.bind.helpers.DefaultValidationEventHandler#handleEvent(javax
	 * .xml.bind.ValidationEvent)
	 */
	@Override
	public boolean handleEvent(ValidationEvent event) {

		log.info(event.getLocator().getObject().getClass().getSuperclass().getName() + " - " + event.getMessage());

		if (model != null)
			this.model.addElement(event.getLocator().getObject().getClass().getName() + " - " + event.getLocator().getLineNumber() + " - " + " - "
					+ event.getMessage());

		return true;
	}

	public ValidationEventHandler(DefaultListModel model) {
		super();
		this.model = model;
	}

	public ValidationEventHandler() {
		super();
	}
}