package de.fzk.iai.ilcd.api.app.common;

import de.fzk.iai.ilcd.api.Configuration;

public class StringMultiLang extends de.fzk.iai.ilcd.api.binding.generated.common.StringMultiLang {

	public StringMultiLang() {
	}

	public StringMultiLang(String value, String lang) {
		this.value = value;
		this.lang = lang;
	}

	public StringMultiLang(String value) {
		this.value = value;
		this.lang = Configuration.getInstance().getLanguage();
	}

}
