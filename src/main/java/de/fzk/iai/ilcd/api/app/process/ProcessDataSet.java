package de.fzk.iai.ilcd.api.app.process;

import de.fzk.iai.ilcd.api.extension.productmodel.ProductModel;

public class ProcessDataSet extends de.fzk.iai.ilcd.api.binding.generated.process.ProcessDataSetType {

	public ProductModel getProductModel() {
		for (Object o : this.getProcessInformation().getOther().getAny()) {
			if (o instanceof ProductModel) {
				return (ProductModel) o;
			}
		}
		return null;
	}

}
