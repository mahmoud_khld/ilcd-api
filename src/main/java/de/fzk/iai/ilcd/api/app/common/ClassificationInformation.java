package de.fzk.iai.ilcd.api.app.common;

public class ClassificationInformation extends de.fzk.iai.ilcd.api.binding.generated.common.ClassificationInformationType {

	/**
	 * 
	 */
	public ClassificationInformation() {
	}

	/**
	 * Convenience constructor
	 * 
	 * @param classification
	 */
	public ClassificationInformation(Classification classification) {
		this.add(classification);
	}

	/**
	 * Convenience method to add a de.fzk.iai.ilcd.api.app.common.Classification
	 * without calling the getClassification() method
	 * 
	 * @param classification
	 */
	public void add(Classification classification) {
		this.getClassification().add(classification);
	}

	/**
	 * Convenience method to get the number of present
	 * de.fzk.iai.ilcd.api.app.common.Classification elements without calling
	 * the getClassification() method
	 * 
	 * @return the size
	 */
	public int size() {
		return this.getClassification().size();
	}
}
