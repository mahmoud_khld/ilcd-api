package de.fzk.iai.ilcd.api.app.process;

import de.fzk.iai.ilcd.api.extension.ef.originatingprocess.OriginatingProcess;

public class Exchange extends de.fzk.iai.ilcd.api.binding.generated.process.ExchangeType {

	public OriginatingProcess getOriginatingProcess() {
		if (this.getOther() !=  null && this.getOther().getAny() !=  null) {
			for (Object o : this.getOther().getAny()) {
				if (o instanceof OriginatingProcess) {
					return (OriginatingProcess) o;
				}
			}
		}
		return null;
	}

}
