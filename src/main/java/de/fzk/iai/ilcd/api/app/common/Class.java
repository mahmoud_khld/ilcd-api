package de.fzk.iai.ilcd.api.app.common;

import java.math.BigInteger;

public class Class extends de.fzk.iai.ilcd.api.binding.generated.common.ClassType {

	public Class() {
	}

	public Class(int level, String value) {
		this.level = BigInteger.valueOf(level);
		this.value = value;
	}
}
