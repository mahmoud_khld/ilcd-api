package de.fzk.iai.ilcd.api.binding.helper;

import org.apache.log4j.Logger;

import com.sun.xml.bind.marshaller.NamespacePrefixMapper;

import de.fzk.iai.ilcd.api.Constants;
import de.fzk.iai.ilcd.api.dataset.ILCDTypes;

public class NamespacePrefixMapperImpl extends NamespacePrefixMapper {
	protected static final Logger log = org.apache.log4j.Logger.getLogger(NamespacePrefixMapperImpl.class);

	@SuppressWarnings("unused")
	private String defaultNameSpaceURI = null;

	public NamespacePrefixMapperImpl(ILCDTypes type) {
		this.defaultNameSpaceURI = type.getNameSpaceURI();
	}

	public NamespacePrefixMapperImpl() {
	}

	/**
	 * Returns a preferred prefix for the given namespace URI.
	 * 
	 * This method is intended to be overrided by a derived class.
	 * 
	 * @param namespaceUri
	 *            The namespace URI for which the prefix needs to be found.
	 *            Never be null. "" is used to denote the default namespace.
	 * @param suggestion
	 *            When the content tree has a suggestion for the prefix to the
	 *            given namespaceUri, that suggestion is passed as a parameter.
	 *            Typicall this value comes from the QName.getPrefix to show the
	 *            preference of the content tree. This parameter may be null,
	 *            and this parameter may represent an already occupied prefix.
	 * @param requirePrefix
	 *            If this method is expected to return non-empty prefix. When
	 *            this flag is true, it means that the given namespace URI
	 *            cannot be set as the default namespace.
	 * 
	 * @return null if there's no prefered prefix for the namespace URI. In this
	 *         case, the system will generate a prefix for you.
	 * 
	 *         Otherwise the system will try to use the returned prefix, but
	 *         generally there's no guarantee if the prefix will be actually
	 *         used or not.
	 * 
	 *         return "" to map this namespace URI to the default namespace.
	 *         Again, there's no guarantee that this preference will be honored.
	 * 
	 *         If this method returns "" when requirePrefix=true, the return
	 *         value will be ignored and the system will generate one.
	 */
	public String getPreferredPrefix(String namespaceUri, String suggestion, boolean requirePrefix) {

		String result = null;

		log.debug("mapping namespaceUri " + namespaceUri + ", suggestion is " + suggestion + ", prefix required: " + requirePrefix);

		if (Constants.NS_SCHEMA_INSTANCE.equals(namespaceUri))
			result = "xsi";

		else if (Constants.NS_COMMON.equals(namespaceUri))
			result = "common";

		else if (namespaceUri.equals(this.defaultNameSpaceURI) && !requirePrefix)
			result = "";

		// else if (!requirePrefix && namespaceUri != null && namespaceUri!="")
		// result = "";

		else if (Constants.NS_PROCESS.equals(namespaceUri))
			result = "process";

		else if (Constants.NS_LCIAMETHOD.equals(namespaceUri))
			result = "lciamethod";

		else if (Constants.NS_FLOW.equals(namespaceUri))
			result = "flow";

		else if (Constants.NS_FLOWPROPERTY.equals(namespaceUri))
			result = "flowProperty";

		else if (Constants.NS_UNITGROUP.equals(namespaceUri))
			result = "unitGroup";

		else if (Constants.NS_SOURCE.equals(namespaceUri))
			result = "source";

		else if (Constants.NS_CONTACT.equals(namespaceUri))
			result = "contact";

		else if (namespaceUri.equals("http://iai.kit.edu/ILCD/List"))
			result = "list";

		else if (Constants.EXTENSION_NS_PRODUCTMODEL.equals(namespaceUri))
			result = "pm";

		else if (Constants.EXTENSION_NS_EF_2018.equals(namespaceUri))
			result = "ef";

		else
			result = null;

		if (result != null) {
			log.debug("returning " + result);
			return result;
		} else {
			log.debug("returning suggested " + result);
			return suggestion;
		}
	}

	/**
	 * Returns a list of namespace URIs that should be declared at the root
	 * element.
	 */
	public String[] getPreDeclaredNamespaceUris() {
		return new String[] { Constants.NS_SCHEMA_INSTANCE };
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.sun.xml.bind.marshaller.NamespacePrefixMapper#getContextualNamespaceDecls
	 * ()
	 */
	@Override
	public String[] getContextualNamespaceDecls() {
		return super.getContextualNamespaceDecls();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seecom.sun.xml.bind.marshaller.NamespacePrefixMapper#
	 * getPreDeclaredNamespaceUris2()
	 */
	@Override
	public String[] getPreDeclaredNamespaceUris2() {
		return super.getPreDeclaredNamespaceUris2();
	}
}
