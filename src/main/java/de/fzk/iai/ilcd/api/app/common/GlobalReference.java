package de.fzk.iai.ilcd.api.app.common;

import de.fzk.iai.ilcd.api.Constants;
import de.fzk.iai.ilcd.api.binding.generated.common.GlobalReferenceTypeValues;

public class GlobalReference extends de.fzk.iai.ilcd.api.binding.generated.common.GlobalReferenceType {

	public GlobalReference() {

	}

	public GlobalReference(GlobalReferenceTypeValues type, String id, String shortDescription) {
		this.type = type;
		this.refObjectId = id;
		this.getShortDescription().add(new STMultiLang(shortDescription));
	}

	public GlobalReference(GlobalReferenceTypeValues type, String id) {
		this.type = type;
		this.refObjectId = id;
	}

	public GlobalReference(GlobalReferenceTypeValues type, String id, String version, String shortDescription) {
		this.type = type;
		this.refObjectId = id;
		this.version = version;
		this.getShortDescription().add(new STMultiLang(shortDescription));
	}

	public GlobalReference(GlobalReferenceTypeValues type, String id, String version, String shortDescription, boolean genURI) {
		this.type = type;
		this.refObjectId = id;
		this.version = version;
		if (shortDescription != null)
			this.getShortDescription().add(new STMultiLang(shortDescription));

		if (genURI) {
			String dirPrefix = null;
			switch (type) {
			case CONTACT_DATA_SET:
				dirPrefix = Constants.FOLDER_NAME_CONTACT;
				break;
			case FLOW_DATA_SET:
				dirPrefix = Constants.FOLDER_NAME_FLOW;
				break;
			case FLOW_PROPERTY_DATA_SET:
				dirPrefix = Constants.FOLDER_NAME_FLOW_PROPERTY;
				break;
			case LCIA_METHOD_DATA_SET:
				dirPrefix = Constants.FOLDER_NAME_LCIA_METHOD;
				break;
			case PROCESS_DATA_SET:
				dirPrefix = Constants.FOLDER_NAME_PROCESS;
				break;
			case SOURCE_DATA_SET:
				dirPrefix = Constants.FOLDER_NAME_SOURCE;
				break;
			case UNIT_GROUP_DATA_SET:
				dirPrefix = Constants.FOLDER_NAME_UNIT_GROUP;
				break;
			case OTHER_EXTERNAL_FILE:
				dirPrefix = Constants.FOLDER_NAME_EXT_DOCS;
				break;
			default:
				break;
			}
			String uri = "../" + dirPrefix + "/" + id + ".xml";
			this.setUri(uri);
		}
	}

	public String getShortDescription(String lang) {
		for (de.fzk.iai.ilcd.api.binding.generated.common.STMultiLang entry : this.shortDescription) {
			if (lang.equals(entry.getLang()))
				return entry.getValue();
		}
		// fallback to first entry if nothing found
		if (this.shortDescription != null && !this.shortDescription.isEmpty()) 
			return this.shortDescription.get(0).getValue();
		else
			return null;
	}
	
}
