/**
 * 
 */
package de.fzk.iai.ilcd.api.validation;

import java.util.ArrayList;
import java.util.List;

import javax.xml.transform.ErrorListener;
import javax.xml.transform.TransformerException;

import org.apache.log4j.Logger;

public class ValidatorListener implements ErrorListener {

	protected static Logger log = org.apache.log4j.Logger.getLogger(ValidatorListener.class);

	private List<String> results = new ArrayList<String>();

	public List<String> getResults() {
		return results;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seejavax.xml.transform.ErrorListener#error(javax.xml.transform.
	 * TransformerException)
	 */
	public void error(TransformerException arg0) throws TransformerException {
		logEvent(arg0);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seejavax.xml.transform.ErrorListener#fatalError(javax.xml.transform.
	 * TransformerException)
	 */
	public void fatalError(TransformerException arg0) throws TransformerException {
		logEvent(arg0);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seejavax.xml.transform.ErrorListener#warning(javax.xml.transform.
	 * TransformerException)
	 */
	public void warning(TransformerException arg0) throws TransformerException {
		logEvent(arg0);
	}

	public void logEvent(TransformerException arg0) throws TransformerException {
		log.info("--" + "--" + arg0.getMessage()); // arg0.getLocator().getLineNumber()
		results.add(arg0.getMessage());
	}

}