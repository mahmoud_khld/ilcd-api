package de.fzk.iai.ilcd.zip;

import java.io.IOException;
import java.io.InputStream;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

import de.fzk.iai.ilcd.api.Constants;

public class ILCDManifest extends Manifest {

	public static final String ILCD_VERSION = "ILCD-Version";

	public static final String CREATED_BY = "Created-By";

	public ILCDManifest(String version) {
		super();
		new ILCDManifest(version, Constants.API_ID);
	}

	public ILCDManifest(String version, String createdBy) {
		super();
		this.getMainAttributes().put(Attributes.Name.MANIFEST_VERSION, "1.0");
		this.getMainAttributes().put(new Attributes.Name(CREATED_BY), createdBy);
		this.getMainAttributes().put(new Attributes.Name(ILCD_VERSION), version);
	}

	public ILCDManifest(Manifest m) {
		super(m);
	}

	public ILCDManifest(InputStream is) throws IOException {
		super(is);
	}

	public String getILCDVersion() {
		return this.getMainAttributes().getValue(new Attributes.Name(ILCD_VERSION));
	}

	public String toString() {
		StringBuffer buf = new StringBuffer();
		Attributes atts = (Attributes) this.getMainAttributes().clone();

		// first line should be manifest version
		buf.append(Attributes.Name.MANIFEST_VERSION);
		buf.append(": ");
		buf.append(atts.get(Attributes.Name.MANIFEST_VERSION));
		buf.append("\n");
		atts.remove(Attributes.Name.MANIFEST_VERSION);

		for (Object key : atts.keySet()) {
			buf.append(key);
			buf.append(": ");
			buf.append(atts.get(key));
			buf.append("\n");
		}

		return buf.toString();
	}
}
